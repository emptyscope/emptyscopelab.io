# Public

## Setup `repo` in Directory

Create root directory in home folder:

Command:

```bash
mkdir src/Emptyscope
```

Create all the projects by using the manifest from the url(`-u`) with the `main` branch.

Command:

```bash
repo init -u https://gitlab.com/emptyscope/emptyscope.gitlab.io.git -m emptyscope-default.xml -b main --config-name
```

If it errored or put you on `HEAD` instead of `main`.  You can run this command to go through and fix all of the repositories.

Command:

```bash
repo forall -c 'echo  $REPO_RREV | sed -e "s%refs/heads/%%" | xargs git checkout'
```

Fetch everything!

Command:

```bash
repo sync
```

If you want to add remotes or change them you can run.  You can run this command to go through and adjust all of the repositories.

Command:

```bash
repo forall -p -c 'git remote set-url upstream emptyscope-gitlab.com:emptyscope/${REPO_PROJECT}.git'
```

```bash
repo forall -p -c 'git remote add origin me-gitlab.com:mtscope/${REPO_PROJECT}.git'
```

## [forall](https://source.android.com/docs/setup/reference/repo#forall)

Executes the given shell command in each project. The following additional environment variables are made available by repo forall:

    REPO_PROJECT is set to the unique name of the project.
    REPO_PATH is the path relative to the root of the client.
    REPO_REMOTE is the name of the remote system from the manifest.
    REPO_LREV is the name of the revision from the manifest, translated to a local tracking branch. Use this variable if you need to pass the manifest revision to a locally executed Git command.
    REPO_RREV is the name of the revision from the manifest, exactly as written in the manifest.

Options:

    -c: Command and arguments to execute. The command is evaluated through /bin/sh and any arguments after it are passed through as shell positional parameters.
    -p: Show project headers before output of the specified command. This is achieved by binding pipes to the command's stdin, stdout, and sterr streams, and piping all output into a continuous stream that is displayed in a single pager session.
    -v: Show messages the command writes to stderr.
