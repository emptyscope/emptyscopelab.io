#! /bin/sh

repo sync
repo forall -c 'echo  $REPO_RREV | sed -e "s%refs/heads/%%" | xargs git checkout'
